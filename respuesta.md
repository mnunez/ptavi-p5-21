
## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
* ¿Cuánto tiempo dura la captura?
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, incluyendo una llamada. 

* Filtra por `sip` para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?
* Y los paquetes con RTP, ¿cuándo se empiezan a enviar?
* Los paquetes RTP, ¿cuándo se dejan de enviar?
* Los paquetes RTP, ¿cada cuánto se envían?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 3

* Componen la captura 1090 paquetes.
* La captura dura 14.4910 segundos.
* La IP que tiene la máquina donde se ha efectuado la captura es 192.168.1.34. Se trata de una IP privada debido a que pertenece al rango C de IP privadas (192.168.0.0 - 192.168.255.255). Los tres primeros bloques identifican la red, mientras que el último identifica los equipos.
* Los tres principales protocolos de nivel de aplicación por número de paquetes son: SIP, RTP y RTCP.
* Otros protocolos que se pueden ver en la jerarquía de protocolos son: Ethernet, IPV4 UDP, STUN e ICMP.
* El protocolo de nivel de aplicación que presenta más tráfico en bytes por segundo es RTP. El tráfico es de 99k bits/s, por lo tanto es aproximadamente 12375 bytes/s.
* Los envíos de los paquetes SIP tienen lugar en los segundos 0, 3 y 14.
* Los paquetes RTP se empiezan a enviar en el segundo 3.9768.
* Los paquetes RTP se dejan de enviar en el segundo 14.4876.
* Los paquetes RTP se envían cada 0.01 segundos aproximadamente.


## Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y "Servidor" a la máquina que proporciona el servicio SIP.

* ¿De qué protocolo de nivel de aplicación son?
* ¿Cuál es la dirección IP de la máquina "Linphone"?
* ¿Cuál es la dirección IP de la máquina "Servidor"?
* ¿En qué máquina está el UA (user agent)?
* ¿Entre qué máquinas se envía cada trama?
* ¿Que ha ocurrido para que se envíe la primera trama?
* ¿Qué ha ocurrido para que se envíe la segunda trama?

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
* ¿Entre qué máquinas se envía cada trama?
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?

Ahora, las tramas 5 y 6.

* ¿De qué protocolo de nivel de aplicación son?
* ¿Entre qué máquinas se envía cada trama?
* ¿Que ha ocurrido para que se envíe la primera de ellas (quinta trama en la captura)?
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (sexta trama en la captura)?
* ¿Qué se indica (en líneas generales) en la parte SDP de cada una de ellas?

Después de la trama 6, busca la primera trama SIP.

* ¿Qué trama es?
* ¿De qué máquina a qué máquina va?
* ¿Para qué sirve?
* ¿Puedes localizar en ella qué versión de Linphone se está usando?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 4

* Las dos primeras tramas de la captura son del protocolo de aplicación SIP.
* La dirección IP de la máquina "Linphone" es 192.168.1.34.
* La dirección IP de la máquina "Servidor" es 212.79.111.155.
* El UA (User Agent) se encuentra en la máquina "Linphone".
* La primera trama de la captura se envía desde la máquina "Linphone" a la máquina "Servidor" y la segunda trama se envía desde la máquina "Servidor" a la máquina "Linphone".
* Para que se envíe la primera trama, la máquina "Linphone" realiza un request (REGISTER) a la máquina "Servidor".
* Para que se envíe la segunda trama, la máquina "Servidor" envía Status: 401 Unauthorized a la máquina "Linphone".
* Las dos tramas siguientes de la captura (tramas 3 y 4) son del protocolo de aplicación SIP.
* La tercera trama de la captura se envía desde la máquina "Linphone" a la máquina "Servidor" y la cuarta trama se envía desde la máquina "Servidor" a la máquina "Linphone".
* Para que se envíe la tercera trama, la máquina "Linphone" realiza un request (REGISTER) a la máquina "Servidor".
* Para que se envíe la cuarta trama, la máquina "Servidor" envía Status: 200 OK a la máquina "Linphone".
* Las tramas 5 y 6 de la captura son del protocolo de aplicación SIP.
* La quinta trama de la captura se envía desde la máquina "Linphone" a la máquina "Servidor" y la sexta trama se envía desde la máquina "Servidor" a la máquina "Linphone".
* Para que se envíe la quinta trama, la máquina "Linphone" realiza un request (INVITE) a la máquina "Servidor".
* Para que se envíe la sexta trama, la máquina "Servidor" envía Status: 200 OK a la máquina "Linphone".
* En la quinta trama se indica en la parte SDP: Owner/Creator usuario con IP 192.168.1.34. Media Description los parámetros en detalle de la sesión RTP (Media type: audio). En la sexta trama se indica en la parte SDP: Owner/Creator usuario con IP 212.79.111.155. Media Description los parámetros en detalle de la sesión RTP (Media type: audio).
* La primera trama SIP después de la trama 6 es la trama 11.
* La trama 11 va de la máquina "Linphone" a la máquina "Servidor".
* La trama 11 sirve para enviar un request: ACK y aceptar las condiciones de envío.
* La versión de Linphone que se está usando es: Linphone/3.12.0 (belle-sip/1.6.3).


## Ejercicio 5. Tramas especiales

Las tramas 7 y 9 parecen un poco especiales:

* ¿De que protocolos son (indica todos los protocolos relevantes por encima de IP).
* De qué máquina a qué máquina van?
* ¿Para qué crees que sirven?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 5

* Las tramas 7 y 9 son de los protocolos UDP (protocolo de transporte) y STUN (protocolo de red).
* Las tramas 7 y 9 van de la máquina "Linphone" a la máquina "Servidor".
* Las tramas 7 y 9 son enviadas para que la máquina "Linphone" encuentre su IP pública, el tipo de NAT en el que se encuentra y el puerto de Internet asociado con el puerto local a través de NAT.


## Ejercicio 6. Registro

Repasemos ahora qué está ocurriendo (en lo que al protocolo SIP se refiere) en las primeras tramas SIP que hemos visto ya:

* ¿Qué dirección IP tiene el servidor que actúa como registrar SIP? ¿Por qué?
* ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?
* ¿Qué método SIP utiliza el UA para registrarse?
* ¿Qué diferencia fundamental ves entre la primera línea del paquete SIP que envía Linphone para registrar a un usuario, y el que hemos estado enviando en la práctica 4?
* ¿Por qué tenemos dos paquetes `REGISTER` en la traza?
* ¿Por qué la cabecera `Contact` es diferente en los paquetes 1 y 3? ¿Cuál de las dos cabeceras es más "correcta", si nuestro interés es que el UA sea localizable?
* ¿Qué tiempo de validez se está dando para el registro que pretenden realizar los paquetes 1 y 3?

Para responder estas preguntas, puede serte util leer primero [Understanding REGISTER Method](https://blog.wildix.com/understanding-register-method/), además de tener presente lo explicado en clase.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 6

* El servidor que actúa como registrar SIP tiene dirección IP 212.79.111.155 porque se trata de una IP pública de clase C (192.0.0.0 a 223.255.255.255).
* Los paquetes SIP se envían al puerto 5060.
* El método SIP que utiliza el UA para registrarse es REGISTER.
* La diferencia fundamental que se ve entre la primera línea del paquete SIP que envía Linphone para registrar a un usuario y el que se enviaba en la práctica 4 es que en el "Message Header" del que se enviaba en la práctica 4 únicamente contenía la cabecera "Expires" mientras que en el "Message Header" del que envía Linphone contiene la cabecera "Expires" además de otro tipo de información (Contact, user-agent, authorization...)
* Se tienen dos paquetes REGISTER en la traza porque el primer paquete es rechazado (el cliente no firma Nonce Value) y el segundo es aceptado.
* La cabecera "Contact" es diferente en los paquetes 1 y 3 porque cada una muestra direcciones IP distintas que se utilizan para contactar al usuario. En el caso del paquete 1 muestra la dirección 192.168.1.34 y en el caso del paquete 3 muestra la dirección 83.38.204.116. La cabecera más "correcta" para que el UA sea localizable es la del paquete 3 ya que muestra una dirección IP pública de clase A (1.0.0.0 a 196.255.255.255), mientras que el paquete 1 muestra una dirección IP privada.
* El tiempo de validez que se está dando para el registro que pretenden realizar los paquetes 1 y 3 es 3600 segundos.
 

## Ejercicio 7. Indicación de comienzo de conversación

* Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?
* ¿En qué paquete el UA indica que quiere comenzar a intercambiar datos de audio con otro? ¿Qué método SIP utiliza para ello?
* ¿Con qué dirección quiere intercambiar datos de audio el UA?
* ¿Qué protocolo (formato) está usando para indicar cómo quiere que sea la conversación?
* En la inidicacion de cómo quiere que sea la conversación, puede verse un campo `m` con un valor que empieza por `audio 7078`. ¿Qué indica el `7078`? ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
* En la respuesta a esta indicacion vemos un campo `m` con un valor que empieza por `audio 27138`. ¿Qué indica el `27138`?  ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
* ¿Para qué sirve el paquete 11 de la trama?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 7

* Además de REGISTER, las intrucciones SIP que entiende el UA son: INVITE (invitación a sesión), ACK (confirmación de respuesta a INVITE), BYE (terminación de sesión), CANCEL (cancela peticiones) y OPTIONS (pregunta las capacidades de los servidores).
* El UA indica que quiere comenzar a intercambiar datos de audio con otro en el paquete 5. Utiliza para ello el método SIP INVITE.
* El UA quiere intercambiar datos de audio con music@iptel.org.
* El protocolo (formato) que está usando para indicar cómo quiere que sea la conversación es SDP.
* En SDP, el valor 7078 en audio 7078 indica el puerto en el cual quiere recibir el cliente los paquetes de audio. La relación que tiene con el destino de algunos paquetes más adelante en la trama es que se indica al servidor dicho destino para que pueda enviar de forma correcta los paquetes.
*  El paquete 11 de la trama sirve para validar las condiciones y así comenzar la comunicación.

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el paquete 12 de la traza:

* ¿De qué máquina a qué máquina va?
* ¿Qué tipo de datos transporta?
* ¿Qué tamaño tiene?
* ¿Cuántos bits van en la "carga de pago" (payload)
* ¿Se utilizan bits de padding?
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
* ¿Cuántos bits/segundo se envían?

Y ahora, las mismas preguntas para el paquete 14 de la traza:

* ¿De qué máquina a qué máquina va?
* ¿Qué tipo de datos transporta?
* ¿Qué tamaño tiene?
* ¿Cuántos bits van en la "carga de pago" (payload)
* ¿Se utilizan bits de padding?
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
* ¿Cuántos bits/segundo se envían?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 8

* El paquete 12 de la traza va de la máquina "Linphone" a la máquina "Servidor".
* El paquete 12 de la traza transporta datos de tipo audio.
* El paquete 12 de la traza tiene un tamaño de 214 bytes (1712 bits).
* En la "carga de pago" (payload) van 1376 bits.
* En el paquete 12 de la traza no se utilizan bits de padding.
* La periodicidad de los paquetes según salen de la máquina origen es de aproximadamente 20 ms.
* Se envían 99k bits/segundo.
* El paquete 14 de la traza va de la máquina "Servidor" a la máquina "Linphone".
* El paquete 14 de la traza transporta datos de tipo audio.
* El paquete 14 de la traza tiene un tamaño de 214 bytes (1712 bits).
* En la "carga de pago" (payload) van 1376 bits.
* En el paquete 14 de la traza no se utilizan bits de paddding.
* La periodicidad de los paquetes según salen de la máquina origen es de aproximadamente 20 ms.
* Se envían 99k bits/segundo.


## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
* ¿Cuántos paquetes se pierden?
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
* Para el flujo desde Servidor hacia LinPhone: ¿cuál es el valor máximo del delta?
* ¿Qué es lo que significa el valor de delta?
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
* ¿Cuáles son esos valores?
* ¿Qué significan esos valores?
* Dados los valores de jitter que ves, ¿crees que se podría mantener una conversación de calidad?

Vamos a ver ahora los valores de un paquete concreto, el paquete 17. Vamos a analizarlo en opción `Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
* El "skew" es negativo, ¿qué quiere decir eso?

Si sigues el jitter, ves que en el paquete 78 llega a ser de 5.52:

* ¿A qué se debe esa subida desde el 0.57 que tenía el paquete 53?

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
* ¿A qué se debe la diferencia?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 9

* Hay dos flujos RTP porque uno tiene como máquina origen la máquina "Linphone" y como destino la máquina "Servidor" y otro tiene como máquina origen la máquina "Servidor"y como destino la máquina "Linphone".
* Se pierden 0 paquetes en ambos flujos.
* Para el flujo desde "Linphone" hacia "Servidor" el valor máximo del delta es 30.834 ms.
* Para el flujo desde "Servidor" hacia "Linphone" el valor máximo del delta es 59.586 ms.
* El valor de delta en RTP significa latencia (suma de retardos temporales).
* En el flujo desde "Linphone" hacia "Servidor" son mayores los valores de jitter máximo y medio.
* En el flujo desde "Linphone" hacia "Servidor" el valor de jitter máximo es 8.445 ms y el valor de jitter medio es 4.153 ms.
* El término de "jitter máximo" significa la máxima variación de tiempo que se produce entre medio de la llegada de paquetes y el término de "jitter medio" significa el valor medio de variaciones de tiempo que se produce entre medio de la llegada de paquetes.
* Dados los valores de jitter que se ven, sí se podría mantener una conversación de calidad debido a que para obtener el mejor rendimiento, el jitter debe mantenerse por debajo de 20 ms (Si excede los 30 ms causará un impacto notable en la calidad de cualquier conversación en tiempo real que mantenga un usuario, provocando distorsiones).

  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` seleccina el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se realiza el `INVITE` que inicia la conversación?
* ¿En qué segundo se recibe el último OK que marca su final?
  
Ahora, pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
* ¿Cuántos en el sentido contrario?
* ¿Cuál es la frecuencia de muestreo del audio?
* ¿Qué formato se usa para los paquetes de audio?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 10

* Analizando la traza como una llamada VoIP completa, se puede observar que la llamada dura 00:00:10 (10 segundos).
* El INVITE que inicia la conversación se realiza en el segundo 3.8778.
* Se recibe el último OK que marca el final de la conversación en el segundo 14.4910.
* Las SSRC que intervienen son: 0x761f98b2 y 0xeeccf15f.
* El número de paquetes que se envía desde "Linphone" a "Servidor" es 524.
* El número de paquetes que se envía desde "Servidor" a "Linphone" es 525.
* La frecuencia de muestreo del audio es 8000 Hz.
* El formato que se usa para los paquetes de audio es g711U.


## Ejercicio 11. Captura de una llamada VoIP

Dirígete a la [web de LinPhone](https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
Lanza LinPhone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú `Ayuda` y seleccionar `Asistente de Configuración de Cuenta`. Al terminar, cierra completamente LinPhone.

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 15 segundos de duración. Recuerda que has de comenzar a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Procura que en la captura haya sólo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos tiene esta captura?
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta ejercicio 11

* La captura realizada tiene dos flujos.
* En el primer flujo (de la máquina 212.79.111.155 a la máquina 212.128.254.3) el valor máximo del delta es 74.297, el valor máximo del jitter es 6.462 y el valor medio del jitter es 0.441; en el segundo flujo (de la máquina 212.128.254.3 a la máquina 212.79.111.155) el valor máximo del delta es 41.167, el valor máximo del jitter es 10.053 y el valor medio del jitter es 6.223.



## Respuesta ejercicio 12

* Llamada realizada usando Linphone desde sip:mnunez@212.128.254.46 hacia sip:paula.n@sip.linphone.org. La llamada dura aproximadamente 15 segundos.
* En la captura realizada hay 12460 paquetes en total.


